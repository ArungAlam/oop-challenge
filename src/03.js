var months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];

var myDays = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum`at', 'Sabtu'];

var date = new Date();

var day = date.getDate();

var month = date.getMonth();

var thisDay = date.getDay(),

    thisDay = myDays[thisDay];

var yy = date.getYear();

var year = (yy < 1000) ? yy + 1900 : yy;
var currentTime = Date.now()
var GMT = -(new Date()).getTimezoneOffset()/60;
var totalSeconds = Math.floor(currentTime/1000);
seconds = ('0' + totalSeconds % 60).slice(-2);
var totalMinutes = Math.floor(totalSeconds/60);
minutes = ('0' + totalMinutes % 60).slice(-2);
var totalHours = Math.floor(totalMinutes/60);
hours = ('0' + (totalHours+GMT) % 24).slice(-2);
var timeDisplay = "[" +thisDay + ', ' + day + ' ' + months[month] + ' ' + year+  ":" + hours + ":" + minutes + ":" + seconds+ "]";


class logger {
    constructor( txt) {
        this.txt = txt
      }
      info(str){
          var a = timeDisplay + "   INFO :" +str;
          return a
      }
      warn(str){
        var a = timeDisplay + "   WARN :" +str;
        return a
    }
    error(str){
        var a = timeDisplay + "   ERROR :" +str;
        return a
    }
    debug(str){
        var a = timeDisplay + "   DEBUG:" +str;
        return a
    }
    alert(str){
        var a = timeDisplay + "   ALERT:" +str;
        return a
    }
    critical(str){
        var a = timeDisplay + "   CRITICAL :" +str;
        return a
    }
    emergency(str){
        var a = timeDisplay + "   EMERGENCY:" +str;
        return a
    }
}
const log = new logger;
console.log(log.info("This is an information about something. "))