var CryptoJS = require("crypto-js");
var AES = require("crypto-js/aes");
var SHA256 = require("crypto-js/sha256");
var MD5= require('crypto-js/md5');
var SHA1= require("crypto-js/sha1");
var SHA512 = require("crypto-js/sha512");

class hash {
    constructor( password) {
      this.password = password
    }

    md5(str){
      return MD5(str).toString(CryptoJS.enc.Hex)
    }
    sha1(str){
      return SHA1(str).toString(CryptoJS.enc.Hex)
    }
    sha256(str){
      return SHA256(str).toString(CryptoJS.enc.Hex)
    }
    sha512(str){
      return SHA512(str).toString(CryptoJS.enc.Hex)
    }
  }
  const Hash = new hash('secret');
console.log(Hash.md5('secret'))
